//
//  Traffic_LightsTests.swift
//  Traffic LightsTests
//

import XCTest

class Traffic_LightsTests: XCTestCase {
    
    var lightsControl: LightsControl!;
    
    override func setUp() {
        super.setUp()
        self.lightsControl = LightsControl.init();
        self.lightsControl.greenRedInterval = 0.3;
        self.lightsControl.amberInterval = 0.05;
        self.lightsControl.turnOn()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        self.lightsControl.turnOff();
        self.lightsControl = nil;
    }
    
    func testLightsFromRedToGreen() {
        XCTAssertTrue(self.lightsControl.state == LightsState.red, "start from red");
        self.lightsControl.turnOff()
        self.lightsControl.state = LightsState.red;
        self.lightsControl.turnOn();
        expectation(forNotification: LightsControl.LightsControlDidUpdate.rawValue, object: nil) { (nofication) -> Bool in
            return self.lightsControl.state == LightsState.green
        }
        waitForExpectations(timeout: self.lightsControl.greenRedInterval + 0.1, handler: nil)
    }
    
    func testLightsFromGreenToAmber() {
        self.lightsControl.turnOff()
        self.lightsControl.state = LightsState.green;
        self.lightsControl.turnOn();
        expectation(forNotification: LightsControl.LightsControlDidUpdate.rawValue, object: nil) { (nofication) -> Bool in
            return self.lightsControl.state == LightsState.amber
        }
        waitForExpectations(timeout: self.lightsControl.greenRedInterval + 0.1, handler: nil)
    }
    
    func testLightsFromAmberToRed() {
        self.lightsControl.turnOff()
        self.lightsControl.state = LightsState.amber;
        self.lightsControl.turnOn();
        expectation(forNotification: LightsControl.LightsControlDidUpdate.rawValue, object: nil) { (nofication) -> Bool in
            return self.lightsControl.state == LightsState.red
        }
        waitForExpectations(timeout: self.lightsControl.amberInterval + 0.1, handler: nil)
    }
    
    func testLightsPowerOff() {
        self.lightsControl.turnOff()
        let expectationNoNotification = expectation(forNotification: LightsControl.LightsControlDidUpdate.rawValue,
                                                    object: nil)
        { (nofication) -> Bool in
            return false
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + self.lightsControl.greenRedInterval * 3) {
            expectationNoNotification.fulfill();
        }
        waitForExpectations(timeout: self.lightsControl.greenRedInterval * 4, handler: nil)
    }
}
