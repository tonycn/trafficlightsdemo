## Traffic Lights

This is a demo project of Traffic Lights.
Please open "Traffic Lights.xcworkspace" to view the project.
There are two targes in the project:

1. Traffic Lights (Application): run this target to check the demo UI application.

2. Traffic LightsTests (Unit Test): run this for unit test.


## Approath
Implement the Lights Control Center (LightsControl.swift) for the lights switch rules,
and send notificaitons to lights view (LightsView.swift) to update lights for states.

## Traffic Lights time interval config
You can update the following lines to shorten the time interval to test.

```
ViewController.swift
    self.lightsControl.greenRedInterval = 30.0;
    self.lightsControl.amberInterval = 5.0;    
```

## Files
- TrafficLightsViewController : demo controller for lights
- LightsControl : the lights switch rules and light states
- LightsView : display lights according to LightsControl

### 
