//
//  LightsView.swift
//  Traffic Lights
//
//  Created by Jianjun on 05/03/2017.
//
//

import Foundation
import UIKit


enum LightsViewDirection {
    case NorthSouth
    case EastWest
}

/**
 Display the traffic lights.
 Two directions supported: NORTH_SOUTH | EAST_WEST.
 */
class LightsView: UIImageView {
    
    var direction:LightsViewDirection!
    var control: LightsControl!;
    
    init(frame: CGRect, control: LightsControl!, direction:LightsViewDirection){
        super.init(frame: frame)
        self.image = #imageLiteral(resourceName: "red");
        self.direction = direction
        self.control = control
        if (direction == LightsViewDirection.EastWest) {
            self.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
        } else {
            self.transform = CGAffineTransform.identity
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.lightsControlDidUpdate(_:)),
                                               name: LightsControl.LightsControlDidUpdate,
                                               object: nil);
    }
    
    func lightsControlDidUpdate(_ noti:NSNotification) {
        switch self.control.state {
        case LightsState.red:
            self.image = #imageLiteral(resourceName: "red")
            break
        case LightsState.amber:
            self.image = #imageLiteral(resourceName: "amber")
            break
        case LightsState.green:
            self.image = #imageLiteral(resourceName: "green")
            break
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
