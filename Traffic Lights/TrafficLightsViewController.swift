//
//  ViewController.swift
//  Traffic Lights
//

import UIKit

import SnapKit


class TrafficLightsViewController: UIViewController {
    
    var toggleBtn:UIButton!

    var lightsControl: LightsControl!;
    var horizontalLights:LightsView!
    var verticalLights:LightsView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.lightsControl = LightsControl.init();
        self.lightsControl.greenRedInterval = 30.0;
        self.lightsControl.amberInterval = 5.0;
        self.setupViews()
        self.lightsControl.turnOn()
        self.toggleBtn.isSelected = self.lightsControl.on
    }
    
    // Power button action
    func powerSwitch() -> Void {
        if self.lightsControl.on == true {
            self.lightsControl.turnOff()
        } else {
            self.lightsControl.turnOn()
        }
        self.toggleBtn.isSelected = self.lightsControl.on
    }
    
    /*
        Create two traffic light views, one is EastWest and the other is NorthSouth.
        Add a button to turn on or off the lights.
     */
    func setupViews() {
        
        let rect = CGRect(
            origin: CGPoint(x: 0, y: 0),
            size: CGSize(width: 64, height: 128)
        )
        // Begin to add two light views
        self.horizontalLights = LightsView.init(frame: rect,
                                                control:self.lightsControl,
                                                direction: LightsViewDirection.EastWest);
        self.view.addSubview(self.horizontalLights)
        self.horizontalLights.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(40)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: rect.size.width, height: rect.size.height))
        }
        
        self.verticalLights = LightsView.init(frame: rect,
                                              control:self.lightsControl,
                                              direction: LightsViewDirection.NorthSouth);
        self.view.addSubview(self.verticalLights)
        self.verticalLights.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(180)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: rect.size.width, height: rect.size.height))
        }
        
        // Begin to add power button
        self.toggleBtn = UIButton.init()
        self.view.addSubview(self.toggleBtn)
        self.toggleBtn.snp.makeConstraints { (make) in
            make.bottom.equalTo(view).offset(-80)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 100, height: 40))
        }
        self.toggleBtn.setTitle("Start", for: UIControlState.normal)
        self.toggleBtn.setTitle("Stop", for: UIControlState.selected)
        self.toggleBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
        self.toggleBtn.setTitleColor(UIColor.init(white: 0, alpha: 0.6), for: UIControlState.selected)
        self.toggleBtn.layer.cornerRadius = 5
        self.toggleBtn.layer.borderWidth = 1
        self.toggleBtn.layer.borderColor = UIColor.black.cgColor
        self.toggleBtn.addTarget(self,
                                 action: #selector(self.powerSwitch),
                                 for: UIControlEvents.touchUpInside);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
