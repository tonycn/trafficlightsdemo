//
//  LightsControl.swift
//  Traffic Lights
//
//  Created by Jianjun on 05/03/2017.
//
//

import Foundation

enum LightsState {
    case red
    case amber
    case green
}

/*
 The Lights Control Center which contains the lights switch rules and states.
 */
class LightsControl: NSObject {
    var state: LightsState {
        didSet {
            NotificationCenter.default.post(name: LightsControl.LightsControlDidUpdate,
                                            object: self.state);
        }
    }
    var greenRedInterval: TimeInterval = 30.0
    var amberInterval: TimeInterval = 5.0
    var on: Bool
    var timer: Timer?
    public static let LightsControlDidUpdate = Notification.Name("LightsControlDidUpdate");

    override init() {
        self.state = LightsState.red;
        self.on = false;
        super.init()
    }

    // Turn on Lights Control Center
    func turnOn() {
        self.on = true;
        self.scheduleTimer();
    }
    
    // Turn off Lights Control Center
    func turnOff() {
        self.on = false;
        self.timer?.invalidate();
        self.timer = nil;
    }
    
    func reset() {
        self.state = LightsState.red;
        self.on = false;
    }
    
    // schedule the timer for traffic lights switching
    func scheduleTimer() {
        var interval:TimeInterval;
        if self.state == LightsState.amber {
            interval = amberInterval;
        } else if self.state == LightsState.green {
            interval = greenRedInterval - amberInterval;
        } else {
            interval = greenRedInterval;
        }
        self.timer = Timer.scheduledTimer(timeInterval: interval,
                                          target: self, selector: #selector(self.lightsTimerDidFire(_:)),
                                          userInfo: nil,
                                          repeats: false)
    }
    
    func switchToNext() {
        switch self.state {
        case LightsState.red:
            self.state = LightsState.green
            break
        case LightsState.amber:
            self.state = LightsState.red
            break
        case LightsState.green:
            self.state = LightsState.amber
            break
        }
        NotificationCenter.default.post(name: LightsControl.LightsControlDidUpdate,
                                              object: self.state);
    }
    
    func lightsTimerDidFire(_ timer: Timer) {
        if self.on == true {
            self.switchToNext()
            self.scheduleTimer()
        }
    }
}
